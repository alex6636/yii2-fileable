<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-May-21
 */

namespace alexs\yii2fileable;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use RuntimeException;
use yii\helpers\Html;

class Fileable extends Behavior
{
    public
        $upload_dir,
        $attribute = 'file', // can be set as "file" | "[0]file" | "file[0]"
        $real_delete_file = true,
        $should_be_deleted_value = 'delete',
        $value_after_delete = null,
        $filename_length = 15,
        $mkdir_mode = 0777;

    protected
        $old_filename = null,
        $real_attribute;

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        if (!$this->upload_dir) {
            throw new InvalidConfigException('An attribute "upload_dir" must be specified.');
        }
        $this->real_attribute = Html::getAttributeName($this->attribute);
    }

    public function events() {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE=>'beforeValidate',
            ActiveRecord::EVENT_AFTER_VALIDATE =>'afterValidate',
            ActiveRecord::EVENT_AFTER_DELETE   =>'afterDelete',
        ];
    }

    public function beforeValidate() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        $this->old_filename = $Model->{$this->real_attribute};
        if ($uploaded_file = UploadedFile::getInstance($Model, $this->attribute)) {
            $Model->{$this->real_attribute} = $uploaded_file;
        }
    }

    public function afterValidate() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        if (!$Model->hasErrors($this->real_attribute)) {
            if ($Model->{$this->real_attribute} instanceof UploadedFile) {
                // upload a new file
                if ($upload_file = $this->uploadFile($Model->{$this->real_attribute})) {
                    if ($this->old_filename && $this->real_delete_file) {
                        // delete an old file
                        $this->deleteFile($this->old_filename);
                    }
                    $Model->{$this->real_attribute} = $upload_file;
                } else {
                    $Model->{$this->real_attribute} = $this->old_filename;
                }
            } elseif ($this->fileShouldBeDeleted()) {
                if (!$Model->isAttributeRequired($this->real_attribute)) {
                    if ($this->real_delete_file && ($delete_filename = $Model->getOldAttribute($this->real_attribute))) {
                        // delete an old file
                        $this->deleteFile($delete_filename);
                    }
                    $Model->{$this->real_attribute} = $this->value_after_delete;
                }
            }
        } else {
            $Model->{$this->real_attribute} = $this->old_filename;
        }
    }

    public function afterDelete() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        if ($this->real_delete_file && $Model->{$this->real_attribute}) {
            $this->deleteFile($Model->{$this->real_attribute});
        }
    }

    public function fileShouldBeDeleted() {
        /** @var ActiveRecord $Model */
        $Model = $this->owner;
        return $Model->{$this->real_attribute} === $this->should_be_deleted_value;
    }

    public function uploadFile(UploadedFile $UploadedFile) {
        if (!$UploadedFile->getHasError()) {
            $dir = $this->makeWritableDir($this->upload_dir);
            $filename = $this->generateFileName($UploadedFile->getExtension());
            $filepath = $dir . '/' . $filename;
            if (!$UploadedFile->saveAs($filepath)) {
                throw new RuntimeException('Unable to upload the file "' . $filepath . '"');
            }
            return $filename;
        }
        return null;
    }

    public function deleteFile($filename) {
        $dir = $this->upload_dir;
        $filepath = $dir . '/' . $filename;
        if (is_file($filepath)) {
            return @unlink($filepath);
        }
        return false;
    }

    public function makeWritableDir($dir) {
        if (!is_dir($dir)) {
            if (!@mkdir($dir, $this->mkdir_mode, true)) {
                throw new RuntimeException('Unable to create the directory "' . $dir . '"');
            }
        }
        return $dir;
    }

    public function generateFileName($extension) {
        $unique_str = substr(md5(uniqid()), 0, $this->filename_length);
        return $unique_str . '.' . $extension;
    }
}