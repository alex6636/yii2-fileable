<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   06-Sep-17
 */

namespace alexs\yii2fileable;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\imagine\Image;
use RuntimeException;

class Imageable extends Fileable
{
    public
        $attribute = 'image',
        $thumbnails = [];

    public function uploadFile(UploadedFile $UploadedFile) {
        if (!$UploadedFile->getHasError()) {
            $dir = $this->makeWritableDir($this->upload_dir);
            $filename = $this->generateFileName($UploadedFile->getExtension());
            $filepath = $dir . '/' . $filename;
            if (!$UploadedFile->saveAs($filepath)) {
                throw new RuntimeException('Unable to upload the file "' . $filepath . '"');
            }
            $this->uploadThumbnails($filename);
            return $filename;
        }
        return NULL;
    }

    public function uploadThumbnails($filename) {
        if (!empty($this->thumbnails)) {
            $dir = $this->makeWritableDir($this->upload_dir);
            foreach ($this->thumbnails as $thumb) {
                if (!empty($thumb['subdir'])) {
                    $subdir = $this->makeWritableDir($dir . '/' . $thumb['subdir']);
                    $filepath_to = $subdir . '/' . $filename;
                } else {
                    $filepath_to = NULL;
                }
                if (!empty($thumb['crop'])) {
                    $this->cropImage($thumb['width'], $thumb['height'], $dir . '/' . $filename, $filepath_to);
                } else {
                    $this->resizeImage($thumb['width'], $thumb['height'], $dir . '/' . $filename, $filepath_to);
                }
            }
        }
    }

    public function deleteFile($filename) {
        $dirs = $this->getImagesDirs();
        foreach ($dirs as $dir) {
            $filepath = $dir . '/' . $filename;
            if (is_file($filepath)) {
                @unlink($filepath);
            }
        }
    }

    public function getImagesDirs() {
        $dir = $this->upload_dir;
        $subdirs = array_filter(ArrayHelper::getColumn($this->thumbnails, 'subdir'));
        foreach ($subdirs as &$subdir) {
            $subdir = $dir . '/' . $subdir;
        }
        return array_merge([$dir], $subdirs);
    }

    public function cropImage($thumb_width, $thumb_height, $path, $to = NULL) {
        Image::crop($path, $thumb_width, $thumb_height)->save($to ?: $path);
    }

    public function resizeImage($thumb_width, $thumb_height, $path, $to = NULL) {
        Image::thumbnail($path, $thumb_width, $thumb_height)->save($to ?: $path);
    }
}
