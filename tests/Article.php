<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2fileable\tests;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string $slug
 * @property string $title
 * @property string $text
 * @property string $file
 * @property string $image
 */

class Article extends ActiveRecord
{
    public function rules() {
        return [
            [['title', 'text'], 'filter', 'filter'=>'trim'],
            [['title'], 'required'],
            [['text'], 'string'],
            ['file', 'file', 'extensions'=>'txt'],
            ['image', 'image', 'extensions'=>['jpg', 'jpeg', 'png', 'gif']],
        ];
    }
}
