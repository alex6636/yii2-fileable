<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2fileable\tests;

class UploadedFileUnableToSave extends \yii\web\UploadedFile
{
    public function saveAs($file, $deleteTempFile = true) {
        return false;
    }
}