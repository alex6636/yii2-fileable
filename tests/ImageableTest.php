<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2fileable\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use alexs\yii2fileable\Imageable;

class ImageableTest extends DatabaseTableTestCase
{
    protected $UploadedFile;
    protected $upload_dir;

    public function testUpload() {
        $Article = new Article;
        $Article->attachBehavior('Imageable', [
            'class'=>Imageable::class,
            'upload_dir'=>$this->upload_dir,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'image'=>$this->UploadedFile,
        ]);
        $Article->save();
        $image = $this->upload_dir . '/' . $Article->image;
        $this->assertFileExists($image);
        @unlink($image);
    }
    
    public function testUploadThumbnails() {
        $Article = new Article;
        $Article->attachBehavior('Imageable', [
            'class'=>Imageable::class,
            'upload_dir'=>$this->upload_dir,
            'thumbnails'=>[
                ['width'=>600, 'height'=>600],
                ['subdir'=>'resized', 'width'=>300, 'height'=>300],
                ['subdir'=>'cropped', 'width'=>150, 'height'=>150, 'crop'=>true],
            ],
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'image'=>$this->UploadedFile,
        ]);
        $Article->save();
        $image = $this->upload_dir . '/' . $Article->image;
        $resized = $this->upload_dir . '/resized/' . $Article->image;
        $cropped = $this->upload_dir . '/cropped/' . $Article->image;
        $this->assertFileExists($image);
        $this->assertFileExists($resized);
        $this->assertFileExists($cropped);

        list($width) = getimagesize($image);
        $this->assertEquals($width, 600);

        list($width) = getimagesize($resized);
        $this->assertEquals($width, 300);

        list($width, $height) = getimagesize($cropped);
        $this->assertEquals($width, 150);
        $this->assertEquals($height, 150);

        @unlink($image);
        @unlink($resized);
        @unlink($cropped);
    }

    public function testDeleteModel() {
        $Article = new Article;
        $Article->attachBehavior('Imageable', [
            'class'=>Imageable::class,
            'upload_dir'=>$this->upload_dir,
            'thumbnails'=>[
                ['width'=>600, 'height'=>600],
                ['subdir'=>'resized', 'width'=>300, 'height'=>300],
                ['subdir'=>'cropped', 'width'=>150, 'height'=>150, 'crop'=>true],
            ],
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'image'=>$this->UploadedFile,
        ]);
        $Article->save();
        $image = $this->upload_dir . '/' . $Article->image;
        $resized = $this->upload_dir . '/resized/' . $Article->image;
        $cropped = $this->upload_dir . '/cropped/' . $Article->image;
        $Article->delete();
        $this->assertFileDoesNotExist($image);
        $this->assertFileDoesNotExist($resized);
        $this->assertFileDoesNotExist($cropped);
    }

    protected function setUp():void {
        parent::setUp();
        $test_file = __DIR__ . '/data/img103.png';
        $this->UploadedFile = new UploadedFile();
        $this->UploadedFile->name = basename($test_file);
        $this->UploadedFile->tempName = $test_file;
        $this->UploadedFile->type = 'image/png';
        $this->UploadedFile->size = filesize($test_file);
        $this->upload_dir = __DIR__ . '/data/uploads';
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
            'file' =>'string DEFAULT NULL',
            'image'=>'string DEFAULT NULL',
        ];
    }
}
