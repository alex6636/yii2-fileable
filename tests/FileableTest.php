<?php
/**
 * @author Alex Sergey (createtruesite@gmail.com)
 * @date   11-Sep-17
 */

namespace alexs\yii2fileable\tests;
use alexs\yii2phpunittestcase\DatabaseTableTestCase;
use alexs\yii2fileable\Fileable;

class FileableTest extends DatabaseTableTestCase
{
    protected $UploadedFile;
    protected $upload_dir;

    public function testUpload() {
        $Article = new Article;
        $Article->attachBehavior('Fileable', [
            'class'=>Fileable::class,
            'upload_dir'=>$this->upload_dir,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'file' =>$this->UploadedFile,
        ]);
        $Article->save();
        $filename = $this->upload_dir . '/' . $Article->file;
        $this->assertFileExists($filename);
        @unlink($filename);
    }

    public function testFileContainErrors() {
        $Article = new Article;
        $Article->attachBehavior('Fileable', [
            'class'=>Fileable::class,
            'upload_dir'=>$this->upload_dir,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'file' =>'oldfile.txt',
        ]);
        $Article->addError('file', 'The file is invalid');
        $this->assertFalse($Article->validate(NULL, false));
        $this->assertEquals('oldfile.txt', $Article->file);
    }

    public function testDeleteModel() {
        $Article = new Article;
        $Article->attachBehavior('Fileable', [
            'class'=>Fileable::class,
            'upload_dir'=>$this->upload_dir,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'file' =>$this->UploadedFile,
        ]);
        $Article->save();
        $filename = $this->upload_dir . '/' . $Article->file;
        $Article->delete();
        $this->assertFileDoesNotExist($filename);
    }

    public function testRealDeleteFile() {
        $Article = new Article;
        $Article->attachBehavior('Fileable', [
            'class'=>Fileable::class,
            'upload_dir'=>$this->upload_dir,
            'real_delete_file'=>false,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'file' =>$this->UploadedFile,
        ]);
        $Article->save();
        $filename = $this->upload_dir . '/' . $Article->file;
        $Article->delete();
        $this->assertFileExists($filename);
        @unlink($filename);
    }

    public function testShouldBeDeleted() {
        $Article = new Article;
        $Article->attachBehavior('Fileable', [
            'class'=>Fileable::class,
            'upload_dir'=>$this->upload_dir,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'file' =>$this->UploadedFile,
        ]);
        $Article->save();
        $filename = $this->upload_dir . '/' . $Article->file;
        $Article->file = 'delete';
        $Article->save();
        $this->assertFileDoesNotExist($filename);
        $this->assertNull($Article->file);
    }

    public function testUnableToSaveException() {
        $this->expectException(\RuntimeException::class);
        $test_file = __DIR__ . '/data/testfile.txt';
        $UploadedFileUnableToSave = new UploadedFileUnableToSave();
        $UploadedFileUnableToSave->name = basename($test_file);
        $UploadedFileUnableToSave->tempName = $test_file;
        $UploadedFileUnableToSave->type = 'text/plain';
        $UploadedFileUnableToSave->size = filesize($test_file);
        $Article = new Article;
        $Article->attachBehavior('Fileable', [
            'class'=>Fileable::class,
            'upload_dir'=>$this->upload_dir,
        ]);
        $Article->setAttributes([
            'id'   =>1,
            'title'=>'First article',
            'text' =>'First article contents',
            'file' =>$UploadedFileUnableToSave,
        ]);
        $Article->save();
    }

    protected function setUp():void {
        parent::setUp();
        $test_file = __DIR__ . '/data/testfile.txt';
        $this->UploadedFile = new UploadedFile();
        $this->UploadedFile->name = basename($test_file);
        $this->UploadedFile->tempName = $test_file;
        $this->UploadedFile->type = 'text/plain';
        $this->UploadedFile->size = filesize($test_file);
        $this->upload_dir = __DIR__ . '/data/uploads';
    }

    protected function getTableName() {
        return 'article';
    }

    protected function getTableColumns() {
        return [
            'id'   =>'pk',
            'title'=>'string NOT NULL',
            'text' =>'string NOT NULL',
            'file' =>'string DEFAULT NULL',
            'image'=>'string DEFAULT NULL',
        ];
    }
}
