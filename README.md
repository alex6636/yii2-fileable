# README #

Behaviors to upload files and images for Yii2

**Note:**<br/>
Use the **FormData** for an Ajax request.<br/>
Don't use the jQuery Form Plugin <http://malsup.com/jquery/form/>.
It doesn't sent the value of hidden input to the server.<br/>
If you want to delete a file, just pass the value **delete**<br/>

### For example ###
```php
<?php
use alexs\yii2fileable\Fileable;
use alexs\yii2fileable\Imageable;
use yii\db\ActiveRecord;
use Yii;

class Article extends ActiveRecord
{
    public function rules() {
        return [
            ['image', 'image', 'extensions'=>['jpg', 'jpeg', 'png', 'gif']],
            ['file', 'file', 'extensions'=>'pdf'],
        ];
    }
    
    // ...

    public function behaviors() {
        return [
            [
                'class'=>Fileable::className(),
                'upload_dir'=>Yii::getAlias('@uploads_dir') . '/files',
            ],
            [
                'class'=>Imageable::className(),
                'upload_dir'=>Yii::getAlias('@uploads_dir') . '/images',
                'thumbnails'=>[
                    ['width'=>600, 'height'=>600],
                    ['subdir'=>'resized', 'width'=>300, 'height'=>300],
                    ['subdir'=>'cropped', 'width'=>150, 'height'=>150, 'crop'=>true],
                ],
            ],
        ];
    }
    // ...
}
```